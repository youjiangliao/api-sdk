<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit679827f1fd0f4f03bfeec0f83df2e1f5
{
    public static $prefixLengthsPsr4 = array (
        'X' => 
        array (
            'Xinpow\\Youapis\\' => 15,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Xinpow\\Youapis\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit679827f1fd0f4f03bfeec0f83df2e1f5::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit679827f1fd0f4f03bfeec0f83df2e1f5::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInit679827f1fd0f4f03bfeec0f83df2e1f5::$classMap;

        }, null, ClassLoader::class);
    }
}
