<?php
namespace Xinpow\YouApis\Package;

use Xinpow\YouApis\Traits\Curl;
use Xinpow\YouApis\Traits\Response;

class Sms {

    use Curl, Response;

    static private $_instance;

    private $_config;

    protected function __construct() {
        $this->_config = require_once(__DIR__ . '/../config.php');
    }

    private function __clone() {
        // ...
    }

    static public function init() {
        if(!self::$_instance instanceof self) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * @api {POST} /sendCode
     *
     * @apiDescription 发送验证码短信
     *
     * @apiName  sendCode
     * @apiGroup Package/Sms
     *
     * @apiParam {Integer} phones   必填，手机号码
     *
     * @apiSuccess {Array} data 返回信息
     * @apiSuccessExample {JSON} Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          "ret": 200,
     *          "msg": "请求成功",
     *          "data": {
     *              "status_code": 200,
     *              "data": {
     *                  "vcode": 5841,
     *                  "lid": 21
     *              },
     *              "message": "短信发送成功"
     *          }
     *      }
     */
    public function sendCode($phone) {
        return $this->reject($this->request("/sms/send_code/{$phone}", [], 'POST'));
    }


    /**
     * @api {POST} /validCode
     *
     * @apiDescription 校检验证码
     *
     * @apiName  validCode
     * @apiGroup Package/Sms
     *
     * @apiParam {Integer} phone   必填，手机号码
     * @apiParam {Integer} vcode   必填，需要校检的验证码
     *
     * @apiSuccess {Array} data 返回信息
     * @apiSuccessExample {JSON} Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          "ret": 200,
     *          "msg": "请求成功",
     *          "data": {
     *              "status_code": 200,
     *              "data": {
     *                  "status": "success"
     *              },
    *               "message": "验证码通过"
     *          }
     *      }
     */
    public function validCode($phone, $vcode) {
        return $this->reject($this->request("/sms/valid_code/{$phone}/{$vcode}", [], 'POST'));
    }

    /**
     * @api {POST} /send
     *
     * @apiDescription 发送常规短信,支持批量发送
     *
     * @apiName  send
     * @apiGroup Package/Sms
     *
     * @apiParam {Array} phones   必填，手机号码, 批量用数组
     * @apiParam {Array} params   必填，短信模版内参数值
     * @apiParam {String}  template_code 必填，短信模版CODE
     *
     * @apiSuccess {Array} data 返回信息
     * @apiSuccessExample {JSON} Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          "ret": 200,
     *          "msg": "请求成功",
     *          "data": {
     *              "status_code": 200,
     *              "message": "短信发送成功"
     *          }
     *      }
     */
    public function send($phones, $params, $template_code) {
        $data = [
            'phones' => is_array($phones) ? join(',', $phones) : $phones,
            'params' => is_array($params) ? json_encode($params, 320) : $params,
            'template_code' => $template_code
        ];
        return $this->reject($this->request('/sms/send', $data,'POST'));
    }
}