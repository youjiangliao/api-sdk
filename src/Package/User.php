<?php
namespace Xinpow\YouApis\Package;

use Xinpow\YouApis\Traits\Curl;
use Xinpow\YouApis\Traits\Response;

class User {

    use Curl, Response;

    static private $_instance;

    private $_config;

    protected function __construct() {
        $this->_config = require_once(__DIR__ . '/../config.php');
    }

    private function __clone() {
        // ...
    }

    static public function init() {
        if(!self::$_instance instanceof self) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * 获取用户数据
     * malikuan
     * @param array $params 查询条件, 通过 name/openid/mobile 查询. example: ['name' => 'alex] / ['openid' => 'xxxxx'] / ['mobile' => 13800138000]
     * @return void
     */
    public function getUser($params) {
        return $this->reject($this->request('/user', $params));
    }

    /**
     * 获取用户列表
     *
     * @param integer $limit 每页显示条数
     * @param integer $page  当前页数
     * @return void
     */
    public function getUserList($limit = 20, $page = 1) {
        return $this->reject($this->request('/user', ['limit' => $limit, 'page' => $page]));
    }

    /**
     * 添加新用户
     *
     * @param array $params 添加的用户数据, 扩展字段为: ext
     * @return void
     */
    public function addUser($params) {
        return $this->reject($this->request('/user', $params, 'post'));
    }

    /**
     * 更新用户
     *
     * @param integer $id 用户 ID
     * @param array   $params 更新的用户数据, 扩展字段为: ext
     * @return void
     */
    public function updateUser($id, $params) {
        return $this->reject($this->request('/user/' . $id, $params, 'update'));
    }

}