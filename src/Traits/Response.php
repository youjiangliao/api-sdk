<?php
namespace Xinpow\YouApis\Traits;

trait Response {

    public function reject($result) {
        $config = require(__DIR__ . '/../config.php');
        // 简单日志
        if(isset($config['debug_key']) && $config['debug_key']) {
            $dir = __DIR__ . '/../../Logs/' . date('Y-m-d');
            @mkdir($dir, 0777, true);
            file_put_contents($dir . '/Logs_' . date('H') . '.txt', json_encode($result, 320) . "\n", FILE_APPEND);
        }
        return $result;
    }

}