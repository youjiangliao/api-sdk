<?php
return [
    // api 集群路径
    'base_path' => 'http://api.nat.com/api/',
    
    // 测试 Api Key, 不为空则使用测试
    'debug_key' => 'e10adc3949ba59abbe56e057f20f883e1',

    // ---------------------------------------
    // 配置信息
    // ---------------------------------------
    'company_id'  => '', // 企业号
    'project_key' => '', // 项目 Key
    'secret_key'  => '', // 密钥

    // ---------------------------------------
    // 请求 Curl 配置
    // ---------------------------------------
    'timeout' => 30, // 超时, 单位: 秒

];
