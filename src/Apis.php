<?php
namespace Xinpow\YouApis;

class Apis {

    protected $_config;

    public function __construct() {
        $this->_config = require_once('config.php');
    }

    protected function __clone() {
        // ...
    }

    public function __call($method, $args) {
        $method = 'Xinpow\\YouApis\\Package\\' . ucfirst($method);
        return $method::init($args);
    }

}
